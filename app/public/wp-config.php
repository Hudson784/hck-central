<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '5IF9RCXWTo5tkLVtjkPNO1cn4CE2/JyqWnJw41g9eNadUJUix5MM/MF5OnANLu8FV7dzdbH+Nj3fP6wK5LDhyg==');
define('SECURE_AUTH_KEY',  'B8dyR6BS4gYsX3AUtMG48vbcrZEhxhpgktHUUpdnXY1TU7khZqOY3AVeum/J6Pn3nbogDdkZU5UaFPx3SUaolw==');
define('LOGGED_IN_KEY',    'GIm9jS4gf9EC3unJnt7C9yKX/4oUTjwTHJbpNnkp4GwiGcLXYQE66B7dzeYXxO0HaxqQ9Pp1ZGexrQsgq303hw==');
define('NONCE_KEY',        'w6sy8SpJLFOxC1YJBSLR2DTSf2U9Yo7DsCp/CNHQqhwWF8TbOy8tU/50EJ8CT6W8sO/H+A8vKk3Fonu9HW1ILQ==');
define('AUTH_SALT',        'lZB5z+aBJCshM4ozgHjFY8jO6IE6WDVOKos/u0qen1z6JLw/wUuXW2Ywf24NyBR9iMksFXtMiwoI2lPdIz7bAw==');
define('SECURE_AUTH_SALT', 'YshALswVRjMdrezV2sHS4FWfDdB0j8CcqzN24jLeilvKH0/Sl5qt8tGTuDlbVrDdToOkybivbhKSSQ2kwQR2LA==');
define('LOGGED_IN_SALT',   'jva8BrNOR9zmaC7EM0CSFoIcIE+pEgESeu3s1/YeihCMDJ+5B94QyH2GJlwfmU5j+oxeFCnMRHDOEk0WwU3SRA==');
define('NONCE_SALT',       'gTet64ubrOnpICG1eVPZ+iyUvi5tRlO1GgVTjhJqd9Ixe6QhZWck/WmKhNBBnJSI2sfPaLSt5oXpBbEhimDcWg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
