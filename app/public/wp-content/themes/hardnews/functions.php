<?php
/*This file is part of HardNews child theme.

All functions of this file will be loaded before of parent theme functions.
Learn more at https://codex.wordpress.org/Child_Themes.

Note: this function loads the parent stylesheet before, then child theme stylesheet
(leave it in place unless you know what you are doing.)
*/

function hardnews_enqueue_child_styles() {
    $min = defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ? '' : '.min';
    $parent_style = 'covernews-style';

    $fonts_url = 'https://fonts.googleapis.com/css?family=Oswald:300,400,700';
    wp_enqueue_style('hardnews-google-fonts', $fonts_url, array(), null);
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/bootstrap/css/bootstrap' . $min . '.css');
    wp_enqueue_style($parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style(
        'hardnews',
        get_stylesheet_directory_uri() . '/style.css',
        array( 'bootstrap', $parent_style ),
        wp_get_theme()->get('Version') );


}
add_action( 'wp_enqueue_scripts', 'hardnews_enqueue_child_styles' );



/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function hardnews_widgets_init()
{

    register_sidebar(array(
        'name'          => esc_html__('Front-page Banner Ad Section', 'hardnews'),
        'id'            => 'home-advertisement-widgets',
        'description'   => esc_html__('Add widgets for frontpage banner section advertisement.', 'hardnews'),
        'before_widget' => '<div id="%1$s" class="widget covernews-widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="widget-title widget-title-1"><span>',
        'after_title' => '</span></h2>',
    ));
}

add_action('widgets_init', 'hardnews_widgets_init');



function hardnews_override_banner_advertisment_function(){
    remove_action('covernews_action_banner_advertisement', 'covernews_banner_advertisement', 10);


}

add_action('wp_loaded', 'hardnews_override_banner_advertisment_function');

/**
 * Overriding Parent theme Advertisment section
 *
 * @since NewsQuare 1.0.0
 *
 */
function hardnews_banner_advertisement()
{

    if (('' != covernews_get_option('banner_advertisement_section')) ) { ?>
        
        <div class="banner-promotions-wrapper">
            <?php if (('' != covernews_get_option('banner_advertisement_section'))):

                $covernews_banner_advertisement = covernews_get_option('banner_advertisement_section');
                $covernews_banner_advertisement = absint($covernews_banner_advertisement);
                $covernews_banner_advertisement = wp_get_attachment_image($covernews_banner_advertisement, 'full');
                $covernews_banner_advertisement_url = covernews_get_option('banner_advertisement_section_url');
                $covernews_banner_advertisement_url = isset($covernews_banner_advertisement_url) ? esc_url($covernews_banner_advertisement_url) : '#';
                $covernews_open_on_new_tab = covernews_get_option('banner_advertisement_open_on_new_tab');
                $covernews_open_on_new_tab = ('' != $covernews_open_on_new_tab) ? '_blank' : '';

                ?>
                <div class="promotion-section">
                    <a href="<?php echo esc_url($covernews_banner_advertisement_url); ?>" target="<?php echo esc_attr($covernews_open_on_new_tab); ?>">
                        <?php echo wp_kses_post($covernews_banner_advertisement); ?>
                    </a>
                </div>
            <?php endif; ?>

        </div>
       
        <!-- Trending line END -->
        <?php
    }

    if (is_active_sidebar('home-advertisement-widgets')): ?>
        <div class="banner-promotions-wrapper">
            <div class="promotion-section">
                <?php dynamic_sidebar('home-advertisement-widgets'); ?>
            </div>
        </div>
    <?php endif;
   
      if (!is_home() && is_front_page()) : ?>

    <div class="test" style="text-align:center;"> 
    <h1 > Welcome to H.C.K Website </h1>
    <p> Here we post updates & highlight videos.</p>
    <p> You can view upcoming tournaments and upload your favorite clips from those tournaments! </p> 
    <p> Make an account and start posting your content today! </p>
    </div> 
    
    <?php
    endif;
}
add_action('covernews_action_banner_advertisement', 'hardnews_banner_advertisement', 10);



function wti_loginout_menu_link( $items, $args ) {
   if ($args->theme_location == 'primary') {
      if (is_user_logged_in()) {
         $items .= '<li class="right"><a href="'. wp_logout_url() .'">'. __("Log Out") .'</a></li>';
      } else {
         $items .= '<li class="right"><a href="'. wp_login_url(get_permalink()) .'">'. __("Log In") .'</a></li>';
      }
   }
   return $items;
}

add_filter( 'wp_nav_menu_items', 'wti_loginout_menu_link', 10, 2 );

function adjust_hck_queries($query) { 

	if(!is_admin() AND is_post_type_archive('news-post') AND $query->is_main_query()){
		$query->set('orderby', 'date'); 
		$query->set('order', 'DESC');
		$query->set('posts_per_page', -1); 
		
		wp_reset_postdata(); 
	}     


	if(!is_admin() AND is_post_type_archive('highlight') AND $query->is_main_query()){
		$query->set('orderby', 'date'); 
		$query->set('order', 'DESC');
		$query->set('posts_per_page', -1); 
		
		wp_reset_postdata(); 
	}
	
	if(!is_admin() AND is_post_type_archive('montage') AND $query->is_main_query()){
		$query->set('orderby', 'date'); 
		$query->set('order', 'DESC');
		$query->set('posts_per_page', -1); 
		
		wp_reset_postdata(); 
	}
	
	if(!is_admin() AND is_post_type_archive('hack') AND $query->is_main_query()){
		$query->set('orderby', 'date'); 
		$query->set('order', 'DESC');
		$query->set('posts_per_page', -1); 
		
		wp_reset_postdata(); 
	}


	if(!is_admin() AND is_post_type_archive('tournament') AND $query->is_main_query()){
		$query->set('meta_key', 'tournament_date');
		$query->set('orderby', 'meta_value_num');
		$query->set('posts_per_page', -1); 
		$query->set('order', 'ASC');
		$query->set('meta_query', array(
			array (
			'key'=>'tournament_date',
			'compare' => '>=', 
			'value' => date('Ymd'),
			'type' => 'numeric'
			)
		));
	       wp_reset_postdata(); 
	}
}

add_action('pre_get_posts', 'adjust_hck_queries'); 

function redirectSubsToFrontEnd() { 
	$currentUser = wp_get_current_user(); 
	$numberOfRoles = count($currentUser->roles); 
	$userRole =$currentUser->roles[0]; 
	if($numberOfRoles == 1 AND $userRole == 'subscriber'){
		wp_redirect(site_url('/')); 
		exit;
	}
	
}

add_action('admin_init', 'redirectSubsToFrontEnd'); 


function noSubsAdminBar() { 
	$currentUser = wp_get_current_user(); 
	$numberOfRoles = count($currentUser->roles); 
	$userRole =$currentUser->roles[0]; 
	if($numberOfRoles == 1 AND $userRole == 'subscriber'){
		show_admin_bar(false);
	}
	
} 

function ourHeaderUrl(){
	return esc_url(site_url('/'));
}

function wpb_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(https://hudsona1.sgedu.site/wp-content/uploads/2020/04/warzone.png);
        height:100px;
        width:300px;
        background-size: 300px 100px;
        background-repeat: no-repeat;
        padding-bottom: 10px;
        }
    </style>
<?php }

function login_background(){
	echo '<style type="text/css">
		body{ background: #; }
			.login form {background: #FFFFFF;}
	</style>';		
}

function login_background_image() {
	echo '<style type="text/css">
		body.login{
			background-image: url( "https://hudsona1.sgedu.site/wp-content/uploads/2020/04/cod.jpg" )!important;
	}
	</style>';
}

function register_text_css_color() {
    echo '<style type="text/css"> 
	    	.login #backtoblog a,
	    	.login #nav a {
    			text-decoration:;
    			color: #FFFFFF;
     	</style>';
}

add_action('login_head', 'register_text_css_color');

add_action('login_head', 'login_background_image');

add_action( 'login_enqueue_scripts', 'wpb_login_logo' );
add_action('login_head', 'login_background');					 	

add_filter('login_headerurl', 'ourHeaderUrl');
add_action('wp_loaded', 'noSubsAdminBar');


