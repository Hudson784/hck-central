<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package CoverNews
 */

get_header();
?>
<style>

.card-img-top {
    width: 100%;
    height: 15vw;
    object-fit: cover;
}
{
  box-sizing: border-box;
}

/* Create two equal columns that floats next to each other */
.column {
  float: left;
  width: 50%;
  padding: 10px;
  height: 300px; /* Should be removed. Only for demonstration */
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}
</style>
<section class="section-block-upper row">

                <div id="primary" class="content-area">
                    <main id="main" class="site-main">
                    <div class="row">

                        <?php
                        if (have_posts()) :

                            if (is_home() && !is_front_page()) : ?>
                                <header>
                                    <h1 class="page-title screen-reader-text"> <?php single_post_title(); ?> </h1>
                                </header>

                            <?php
                            endif;

                            /* Start the Loop */
                            while (have_posts()) : the_post();
                            ?>
                  
                            <?php 
                                      // Custom Content begins here
                            get_template_part('template-parts/content','archives');
                            
                            $homepageUpdates = new WP_Query(array(
				'posts_per_page' => 1,
				'post_type' => 'highlight'
			    ));
		
			  if ($homepageUpdates->have_posts()){
			        echo "<hr>"; 
			        ?>	
			          <div class="column" style="background-color:#FFFFFF;">	
			          <div class ="widget-title">	        
			       		<marquee behavior="scroll" direction="left" scrollamount ="10">			        	
						<?php echo "<h2> Latest Highlight! </h2>"; ?>
					</marquee>	
				</div>	
				<?php
			  }
			   while($homepageUpdates->have_posts()){
				$homepageUpdates->the_post();?>
				<?php if (get_post_type() == 'highlight') {
					?>
				<div class="card border-dark mb-3 border-color: black" style="max-width: 18rem;">
			          <a  href="<?php the_permalink(); ?>" class="card-title"> <h4> <?php the_title(); ?> </h4> </a>
			          <div class="card-subtitle"> <h6 class="text-muted">  </h6>  </div>
				  <img class="card-img-top" src="<?php the_post_thumbnail_url();?>">
				  <div class="card-body">
				  
				  </div>
				  <div class="card-footer">
      				       <h6 class="text-muted">
	      				      <?php echo "By ".get_the_author(); ?>
					</h6>
    				 </div>
				</div>
				<?php
			  	}
			  	
			  }	?>
			  	<a href="<?php the_permalink(); ?>"class="btn btn-primary">
				 View Highlight
				</a>
			  </div>
			  <?php	
		

	                 wp_reset_postdata();
		         $currentTournaments = new WP_Query(array( 
				'posts_per_page' => 1, 
				'post_type' => 'tournament',
				'meta_key' => 'tournament_date',
				'orderby' => 'meta_value_num',
				'order' => 'ASC',
				'meta_query' => array(
					array(
					'key' => 'tournament_date', 
					'compare' => '>=', 
					'value' => date('Ymd'), 
					'type' => 'numeric'
					)
				)
			));	
			if ($currentTournaments->have_posts()){

			        ?>	
			        <div class="column" style="background-color:#FFFFFF;">	
			         <div class ="widget-title">	        
			        	<marquee behavior="scroll" direction="left" scrollamount ="10">
						<?php echo "<h2> Recent Tournament! </h2>"; ?>
					</marquee>	
				</div>	
				<?php	
			}
			while ($currentTournaments->have_posts()) {
				$currentTournaments->the_post(); 
				?>
				<div class="card border-dark mb-3 border-color: black" style="max-width: 18rem;">
			          <a  href="<?php the_permalink(); ?>" class="card-title"> <h4> <?php the_title(); ?> </h4> </a>
			          <div class="card-subtitle"> <h6 class="text-muted"> <?php
						$tournamentDate = new DateTime(get_field('tournament_date'));
						echo "Starting: ";
						echo $tournamentDate->format('l'); 
						echo ", "; 
						echo $tournamentDate->format('M');
						echo " ";  
						echo $tournamentDate->format('d');
						echo ", "; 
						echo $tournamentDate->format('Y');
						echo " at "; 
						echo get_field('tournament_time'); 
					      ?> </h6>  </div>
				  <img class="card-img-top" src="<?php the_post_thumbnail_url();?>">
				  <div class="card-body">
				  
				  </div>
				  <div class="card-footer">
      				       <h6 class="text-muted">
	      				      <?php echo "By ".get_the_author(); ?>
					</h6>
    				 </div>
				</div>
			<?php
		}
		?>
			<a href="<?php the_permalink(); ?>" class="btn btn-primary">
		View Tournament
		</a>
		</div>
	
		<?php
		wp_reset_postdata();
	
                            
                            // End of custom content

                                /*
                                 * Include the Post-Format-specific template for the content.
                                 * If you want to override this in a child theme, then include a file
                                 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                 */
                           


                            endwhile; ?>
                            <div class="col col-ten">
                                <div class="covernews-pagination">
                                    <?php covernews_numeric_pagination(); ?>
                                </div>
                            </div>
                        <?php

                        else :

                            get_template_part('template-parts/content', 'none');

                        endif; ?>

                        </div>
                    </main><!-- #main -->
                </div><!-- #primary -->

                <?php
                get_sidebar();
                ?>

    </section>
<?php
get_footer();
