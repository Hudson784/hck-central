<?php
function hckcentral_post_types() {
register_post_type('highlight',array(
	'supports' => array('title','editor','excerpt', 'thumbnail', 'post-formats'),
	'capability_type' => 'highlight',
	'map_meta_cap'=> true, 
	'rewrite' => array('slug' => 'highlights'),
	'has_archive' => true, 
	'public' => true,
	'labels' => array( 
	 'name' => "Highlights", 
	 'add_new_item' => 'Add New Highlight', 
	 'edit_item' => 'Edit Highlight', 
	 'all_items' => 'All Highlights', 
	 'singular' => 'Highlight' 
	),
	'menu_icon' => 'dashicons-star-empty'
));

register_post_type('news-post',array(
	'supports' => array('title','editor', 'thumbnail', 'post-formats'),
	'capability_type' => 'news-post',
	'map_meta_cap'=> true,
	'rewrite' => array('slug' => 'news'),
	'has_archive' => true, 
	'public' => true,
	'labels' => array( 
	 'name' => "News", 
	 'add_new_item' => 'Add News Post', 
	 'edit_item' => 'Edit News Post', 
	 'all_items' => 'All News Posts', 
	 'singular' => 'News Post' 
	),
	'menu_icon' => 'dashicons-format-aside'
));
register_post_type('how-to-video',array(
	'supports' => array('title','editor', 'thumbnail', 'post-formats'),
	'capability_type' => 'how-to-video', 
	'map_meta_cap'=> true,
	'rewrite' => array('slug' => 'how-to-videos'),
	'has_archive' => true, 
	'public' => true,
	'labels' => array( 
	 'name' => "How-to-Videos", 
	 'add_new_item' => 'Add How-to-Video', 
	 'edit_item' => 'Edit How-to-video', 
	 'all_items' => 'All How-to-Videos', 
	 'singular' => 'How-to-Video' 
	),
	'menu_icon' => 'dashicons-video-alt3'
));

register_post_type('tournament',array(
	'supports' => array('title','editor', 'thumbnail', 'post-formats'),
	'capability_type' => 'tournament', 
	'map_meta_cap'=> true,
	'rewrite' => array('slug' => 'tournaments'),
	'has_archive' => true, 
	'public' => true,
	'labels' => array( 
	 'name' => "Tournaments", 
	 'add_new_item' => 'Add Tournament', 
	 'edit_item' => 'Edit Tournament', 
	 'all_items' => 'All Tournaments', 
	 'singular' => 'Tournament' 
	),
	'menu_icon' => 'dashicons-buddicons-buddypress-logo'
));


register_post_type('montage',array(
	'supports' => array('title','editor', 'thumbnail', 'post-formats'), 
	'capability_type' => 'montage',
	'map_meta_cap'=> true,
	'rewrite' => array('slug' => 'montages'),
	'has_archive' => true, 
	'public' => true,
	'labels' => array( 
	 'name' => "Montages", 
	 'add_new_item' => 'Add Montage', 
	 'edit_item' => 'Edit Montage', 
	 'all_items' => 'All Montages', 
	 'singular' => 'Montage' 
	),
	'menu_icon' => 'dashicons-video-alt3'
));


register_post_type('hack',array(
	'supports' => array('title','editor', 'thumbnail', 'post-formats'),
	'capability_type' => 'hack',
	'map_meta_cap'=> true,
	'rewrite' => array('slug' => 'hacks'),
	'has_archive' => true, 
	'public' => true,
	'labels' => array( 
	 'name' => "Hacks", 
	 'add_new_item' => 'Add Hack', 
	 'edit_item' => 'Edit Hack', 
	 'all_items' => 'All Hacks', 
	 'singular' => 'Hack' 
	),
	'menu_icon' => 'dashicons-video-alt3'
));

	
}
add_action('init', 'hckcentral_post_types');
?>